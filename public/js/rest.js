$(document).ready(function(){
	$('.enviar').click(function(){
		var message = $('.input-message').val();
		$.ajax({
			url : 'http://127.0.0.1:8085/api',
			method : 'POST',
			data: { message : message },
			dataType : 'json',
			success : function(data) {
				var html = "";
					$.each(data, function(id,value) {
				    	html += "<li class='list-group-item' id='"+id+"' > "+value.message+" </li>";
					});
	            $('.return').prepend(html);
	        },
			error : function(xhr, status) {
				alert('Desculpa, aconteceu um problema!');
			},
			complete : function(xhr, status) {
				$('.input-message').val('');
			}
		})
	});

	
	$.ajax({
		url : 'http://127.0.0.1:8085/api',
		method : 'GET',
		dataType : 'json',
		success : function(data) {
			var html = "";
			$.each(data, function(k, v) {
				$.each(v, function(id,value) {
			    	html += "<li class='list-group-item' id='"+id+"' > "+value.message+" </li>";
				});
			});
            $('.return').append(html);
        },
		error : function(xhr, status) {
			alert('Desculpa, aconteceu um problema!');
		}
	})
});