<?php

namespace Spike\Service;

use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;


class Spike extends AbstractService
{

    protected $view;
    
    public function __construct(EntityManager $em, $view) 
    {
        parent::__construct($em);
        
        $this->entity = "Spike\Entity\Spike";
        $this->view = $view;
    }
    
   
}
