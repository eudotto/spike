<?php

namespace Spike\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;

use Spike\Form\Spike as FormSpike;

class IndexController extends AbstractActionController
{
    public function registerAction() 
    {
    	$form = new FormSpike;
        $request = $this->getRequest();
        
        if($request->isPost())
        {
            $form->setData($request->getPost());
            if($form->isValid())
            {
                $service = $this->getServiceLocator()->get("Spike\Service\Spike");
                if($service->insert($request->getPost()->toArray())) 
                {
                    $fm = $this->flashMessenger()
                            ->setNamespace('Spike')
                            ->addMessage("Mensagem cadastrado com sucesso");
                }
                
                return $this->redirect()->toRoute('spike-register');
            }
        }
        
        $messages = $this->flashMessenger()
                ->setNamespace('Spike')
                ->getMessages();
        
        return new ViewModel(array('form'=>$form,'messages'=>$messages));
    }
}
