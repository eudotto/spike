<?php

namespace Spike\Controller;

use Zend\View\Model\ViewModel;

class SpikeController extends CrudController 
{

    public function __construct() 
    {
        $this->entity = "Spike\Entity\Spike";
        $this->form = "Spike\Form\Spike";
        $this->service = "Spike\Service\Spike";
        $this->controller = "spike";
        $this->route = "spike-admin";
    }
 
     
}
