<?php

namespace Spike\Form;

use Zend\InputFilter\InputFilter;

class SpikeFilter extends InputFilter
{
    
    public function __construct() 
    {
        
        $this->add(array(
           'name'=>'message',
            'required'=>true,
            'filters' => array(
                array('name'=>'StripTags'),
                array('name'=>'StringTrim'),
            ),
            'validators' => array(
                array('name'=>'NotEmpty','options'=>array('messages'=>array('isEmpty'=>'Não pode estar em branco')))
            )
        ));
    }   
}