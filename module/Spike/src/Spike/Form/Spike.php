<?php

namespace Spike\Form;

use Zend\Form\Form;

class Spike  extends Form
{

    public function __construct($name = null, $options = array()) {
        parent::__construct('spike', $options);
        
        $this->setInputFilter(new SpikeFilter());
        $this->setAttribute('method', 'post');
        
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $message = new \Zend\Form\Element\Text("message");
        $message->setLabel("Mensagem: ")
                ->setAttribute('placeholder','Mensagem');
        $this->add($message);
       
        $csrf = new \Zend\Form\Element\Csrf("security");
        $this->add($csrf);
        
        $this->add(array(
            'name' => 'submit',
            'type'=>'Zend\Form\Element\Submit',
            'attributes' => array(
                'value'=>'Salvar',
                'class' => 'btn-success'
            )
        ));
                
       
    }
    
}
