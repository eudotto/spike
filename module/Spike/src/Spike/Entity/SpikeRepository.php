<?php

namespace Spike\Entity;

use Doctrine\ORM\EntityRepository;

class SpikeRepository extends EntityRepository 
{
    
    public function findArray()
    {
        $messages = $this->findBy(array(), array('createdAt' => 'DESC'));
        $return = array();
        foreach($messages as $message)
        {
            $return[$message->getId()]['id'] = $message->getId();
            $return[$message->getId()]['message'] = $message->getMessage();
            $return[$message->getId()]['created_at'] = $message->getCreatedAt();
        }
        
        return $return;
    }
    
    public function toArray(){
    	
    }

}
