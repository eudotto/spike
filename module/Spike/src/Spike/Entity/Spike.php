<?php

namespace Spike\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * Spike
 *
 * @ORM\Table(name="spike")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Spike\Entity\SpikeRepository")
 */
class Spike {
	/**
	 * @ORM\Id
	 * @ORM\Column(name="id", type="guid")
	 * @ORM\GeneratedValue(strategy="NONE")
	 *
	 * @var string
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="message", type="string", length=255, nullable=false)
	 */
	private $message;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_at", type="datetime", nullable=false)
	 */
	private $createdAt;
	public function __construct(array $options = array()) {
		/*
		 * $hydrator = new Hydrator\ClassMethods;
		 * $hydrator->hydrate($options, $this);
		 */
		(new Hydrator\ClassMethods ())->hydrate ( $options, $this );
		
		$this->id = $this->gen_uuid();
		$this->createdAt = new \DateTime ( "now" );
	}
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getMessage() {
		return $this->message;
	}
	public function setMessage($message) {
		$this->message = $message;
		return $this;
	}
	public function getCreatedAt() {
		return $this->createdAt;
	}
	public function setCreatedAt() {
		$this->createdAt = new \DateTime ( "now" );
	}
	public function toArray() {
		return (new Hydrator\ClassMethods ())->extract ( $this );
	}
	
	public function gen_uuid() {
		return sprintf ( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x', 
				// 32 bits for "time_low"
				mt_rand ( 0, 0xffff ), mt_rand ( 0, 0xffff ), 
				
				// 16 bits for "time_mid"
				mt_rand ( 0, 0xffff ), 
				
				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 4
				mt_rand ( 0, 0x0fff ) | 0x4000, 
				
				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				mt_rand ( 0, 0x3fff ) | 0x8000, 
				
				// 48 bits for "node"
				mt_rand ( 0, 0xffff ), mt_rand ( 0, 0xffff ), mt_rand ( 0, 0xffff ) );
	}
}
