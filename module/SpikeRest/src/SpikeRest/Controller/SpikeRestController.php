<?php

namespace SpikeRest\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class SpikeRestController extends AbstractRestfulController
{

    // Listar - GET
    public function getList()
    {
        
        $em = $this->getServiceLocator()->get("Doctrine\ORM\EntityManager");
        $repo = $em->getRepository("Spike\Entity\Spike");
        
        $data = $repo->findArray();
     
        return new JsonModel(array('data'=>$data));
        
    }
    
    // Retornar o registro especifico - GET
    public function get($id)
    {
        $em = $this->getServiceLocator()->get("Doctrine\ORM\EntityManager");
        $repo = $em->getRepository("Spike\Entity\Spike");
        
        $data = $repo->find($id)->toArray();
        
        return new JsonModel(array('data'=>$data));
        
    }
    
    // Insere registro - POST
    public function create($data)
    {
        $spikeService = $this->getServiceLocator()->get("Spike\Service\Spike");
        
        if($data)
        {
            $spike = $spikeService->insert($data);
            if($spike)
            {
                return new JsonModel(array('data'=>array('id'=>$spike->getId(),'message'=>$spike->getMessage())));
            }
            else
            {
                return new JsonModel(array('data'=>array('success'=>false)));
            }
        }
        else
            return new JsonModel(array('data'=>array('success'=>false)));
    }
    
    // alteracao - PUT
    public function update($id, $data)
    {
        $data['id'] = $id;
        $spikeService = $this->getServiceLocator()->get("Spike\Service\Spike");
        
        if($data)
        {
            $spike = $spikeService->update($data);
            if($spike)
            {
                return new JsonModel(array('data'=>array('id'=>$spike->getId(),'success'=>true)));
            }
            else
            {
                return new JsonModel(array('data'=>array('success'=>false)));
            }
        }
        else
            return new JsonModel(array('data'=>array('success'=>false)));
    }
    
    // delete - DELETE
    public function delete($id)
    {
        $spikeService = $this->getServiceLocator()->get("Spike\Service\Spike");
        
        if($spikeService->delete($id))
        {
            return new JsonModel(array('data'=>array('success'=>true)));
        }
        else
            return new JsonModel(array('data'=>array('success'=>false)));
    }
    
}
