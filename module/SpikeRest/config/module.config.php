<?php

namespace SpikeRest;


return array(
    'controllers' => array(
        'invokables' => array(
            'SpikeRest\Controller\SpikeRest' => 'SpikeRest\Controller\SpikeRestController'
        )
    ),
    'router' => array(
        'routes' => array(
            'sonuser-rest' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api[/:id]',
                    'constraints' => array(
                        'id' => '[a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'SpikeRest\Controller\SpikeRest'
                    )
                )
            )
        )
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    )
);